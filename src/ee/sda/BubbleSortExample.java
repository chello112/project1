package ee.sda;

import java.util.Scanner;

public class BubbleSortExample {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Please, give some a set of natural numbers:");

        // 23,2,45,56,17
        // 2, 23, 45, 56, 17
        String line = scan.nextLine();
        String[] stringArray = line.split(",");

        int[] arr = new int[stringArray.length];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = Integer.parseInt(stringArray[i]);
        }

        // Bubble sort started
        // 23,2,45,56,17
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length-1-i; j++) {
                // 23 and 2
                if(arr[j]>arr[j+1])
                {
                    int temp=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=temp;

                    // temp = 23;
                    // 23 = 2;
                    // 2 = 23;
                }
            }
        }
        // Bubble sort ends

        System.out.println("Your numbers are sorted now:");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+ " ");
        }
    }
}
